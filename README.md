# ml [ML_(programming_language)](https://en.m.wikipedia.org/wiki/ML_(programming_language))

[[_TOC_]]

# Documentation
* [*Monadic Programming in ML*
  ](https://www.cs.umd.edu/~mwh/papers/monadic.pdf)
  2011 Nikhil Swamy, Nataliya Guts, Daan Leijen, Michael Hicks

## Yaron Minsky "Effective ML" serie
* [*Effective ML Revisited*](https://blog.janestreet.com/effective-ml-revisited/)
  2011-03 Yaron Minsky
* [*Effective ML video*](https://blog.janestreet.com/effective-ml-video/)
  2010-08 Yaron Minsky
* [*Effective ML*](https://blog.janestreet.com/effective-ml/)
  2010-04 Yaron Minsky
